<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>New User</title>
<style>
h1 {
	font-size: 450%;
	color: rgba(117, 159, 255, 0.75);
	text-shadow: 0px 0px 45px white;
	text-align: center;
}

h2 {
	font-size:200%;
	color: rgb(250, 253, 255);
	text-shadow: 0px 0px 3px black;
	text-align: center;
}
div.form {
	background-color:rgba(156, 174, 204, 0.4);
	border-radius: 10px;
	width:50%;
	text-align: center;
	margin:auto;
	margin-top:5px;
	padding-top:1px;
	padding-bottom:5px;
}

span.star {
	text-shadow:0 0 10px rgba(0, 38, 77, 0.8);
}

</style>
</head>

<body style="background-color:rgb(179, 217, 255);">

<h1><span class="star">&#127788</span> Fill in new user info <span class="star"> &#127788</span></h1>

<div class="form">
	<form action="/first/new-user-servlet" method="POST" style="vertical-align:middle;text-align:center;">
		<h2>First Name</h2>
		<input type="text" name="firstName" style="background-color:rgba(255,255,255,0.75);">
		<br />
		<h2>Last Name</h2>
		<input type="text" name="lastName" style="background-color:rgba(255,255,255,0.75);">
		<br />
		<h2>E-Mail Address</h2>
		<input type="text" name="email" style="background-color:rgba(255,255,255,0.75);">
		<br />
		<h2>Username</h2>
		<input type="text" name="userName" style="background-color:rgba(255,255,255,0.75);">
		<br />
		<h2>Favorite Breakfast Food</h2>
		<input type="text" name="breakfast" style="background-color:rgba(255,255,255,0.75);margin-bottom:2em;">
		<br />
	 	<input type="submit" name="createAccount" value="Create account" style="margin-bottom:1em;">
	</form>
	<h3 style="font-size:85%;color:rgb(43, 58, 84);">Instead of asking for a password, we just use your favorite breakfast food to identify who you are :)</h3>
</div>

</body>

</html>