<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="hello.jdbc.MySqlConnection"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Login</title>
<style>
h1 {
	font-size: 450%;
	color: rgba(117, 159, 255, 0.75);
	text-shadow: 0px 0px 45px white;
	text-align: center;
}

h2 {
	font-size:200%;
	color: rgb(250, 253, 255);
	text-shadow: 0px 0px 3px black;
	text-align: center;
}

#error {
	color: rgb(128, 0, 255);
	text-shadow: 0px 0px 1px black;
	font-size: 250%;
	margin:0em;
}
div.form {
	background-color:rgba(156, 174, 204, 0.4);
	border-radius: 10px;
	width:50%;
	text-align: center;
	margin:auto;
	margin-top:5px;
	padding-top:1px;
	padding-bottom:5px;
}

</style>
</head>

<body style="background-color:rgb(179, 217, 255);">

<h1> &#127783 Log in please &#127783</h1>

<c:choose>
	<c:when test="${loginSuccess == false}">
	<h2 id="error" style="margin-top:-1em;"> &#128557 Sorry, we can't find you! &#128557</h2>
	<h2 id="error">Try again or create a new account</h2>
	<br />
	</c:when>
</c:choose>

<div class="form">
<form action="LoginServlet" method="POST" id="dreamform" style="vertical-align:middle;text-align:center;">
	<h2>Username</h2>
	<input type="text" name="username" style="background-color:rgba(255,255,255,0.75);">
	<br />
	<h2>Favorite Breakfast Food</h2>
	<input type="text" name="password" style="background-color:rgba(255,255,255,0.75);margin-bottom:2em;">
	<br />
	<input type="submit" name="logIn" value="Log in" style="margin-bottom:1em;" form="dreamform">
	<br />
	<input type="submit" name="createAccount" value="Create account" style="margin-bottom:2em;" form="createAccount">
	
</form>
<form action="/first/new-user" method="POST" id="createAccount">
</form>
<h3 style="font-size:90%;color:rgb(43, 58, 84);">Instead of asking for a password, we just use your favorite breakfast food to identify who you are :)</h3>
</div>

</body>

</html>