<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ page import="hello.misc.Time" %>
<%@ page import="hello.beans.Entry" import="hello.jdbc.DreamDatabase" import="hello.misc.Util" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Redo Entry</title>
<style>
h1 {
	font-size: 450%;
	color: rgba(117, 159, 255, 0.75);
	text-shadow: 0px 0px 45px white;
	text-align: center;
}

h2.form {
	font-size:200%;
	color: rgba(255, 255, 255, 1);
	text-shadow: 0px 0px 3px black;
	text-align: center;
}
div {
	background-color:rgba(156, 174, 204, 0.4);
	border-radius: 10px;
	width:50%;
	text-align: center;
	margin:auto;
	margin-top:5px;
	padding-top:1px;
	padding-bottom:20px;
}

</style>
</head>

<body style="background-color:rgb(179, 217, 255);">

<h1>&#128564 Dream Journal &#128564</h1>
<h2 style="margin-top:-2em;color:rgba(0, 64, 128, 0.9);text-align:center;text-shadow:0 0 2px rgba(0,0,0,0.5);"><%= Time.getJournalDate() %></h2>
<div>
	<form action="RedoEntryServlet" method="POST" id="dreamform" style="vertical-align:middle;text-align:center;">
		<h2 class="form">How are you feeling today?</h2>
		<textarea style="height:4em;width:20em;background-color:rgba(255,255,255,0.75);"
				name="feeling" form="dreamform">${todaysEntry.feeling }</textarea>
		<br />
		<h2 class="form">Did you dream last night?</h2>
		<input type="radio" name="didDream" value="yes" ${todaysEntry.didDream == 'yes' ? 'checked' : ''} style="margin:5px;"> Yes
		<input type="radio" name="didDream" value="no" ${todaysEntry.didDream == 'no' ? 'checked' : ''} style="margin:5px;"> No
		<br />
		<h2 class="form">If you did, then what did you dream about?</h2>
		<h2 class="form">Be as detailed or as vague as you want</h2>
		<textarea style="height:10em;width:50em;background-color:rgba(255,255,255,0.75);"
				name="entry" form="dreamform">${todaysEntry.entry }</textarea>
		<br />
		<br />
		<input type="submit" name="submit" style="margin:10px;">
	</form>
</div>

</body>

</html>