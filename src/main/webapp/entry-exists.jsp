<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Entry Exists</title>
<style>
h1 {
	font-size: 450%;
	color: rgba(117, 159, 255, 0.85);
	text-shadow: 0px 0px 45px rgba(255,255,255, 0.5);
	text-align: center;
}

h2 {
	font-size:200%;
	color: rgb(250, 253, 255);
	text-shadow: 0px 0px 3px black;
	text-align: center;
}

div.form {
	background-color:rgba(255, 255, 255, 0.4);
	border-radius: 10px;
	width:50%;
	text-align: center;
	margin:auto;
	margin-top:5px;
	padding-top:1px;
	padding-bottom:5px;
}
</style>
</head>
<body style="background-color:rgb(0, 64, 128);">
<h1>&#127772 Already Submitted Today's Entry &#127771</h1>
<div class="form">
<form action="EntryExistsServlet" method="POST">
<h2>Hey, you've already submitted a dream journal entry for last night.</h2>
<h2>Would you like to edit your entry?</h2>
<input type="submit" name="answer" value="yes">
<input type="submit" name="answer" value="no">

</form>


</div>
</body>
</html>