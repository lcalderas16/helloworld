package hello.jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * This class is responsible for getting connections to MySql databases
 * 
 * @author lcalderas16
 */
public class MySqlConnection {
    
    
    static String hostname = "10.96.176.117";
    static String database = "dreams";
    static String username = "aws";
    static String password = "root";
    static int port = 3306;
    
    /**
     * Get the MySql connection to the dreams database on the AWS instance
     * @return
     * @throws SQLException
     */
    public static Connection getMySqlConnection() throws SQLException {
        return getMySqlConnection(hostname, database, username, password);
    }
    
    /**
     * Get a MySql Connection to a database
     * @param hostname The IP address of the host machine with the database
     * @param database The name of the database
     * @param username The username of the database user accessing from a specific machine
     * @param password The password for the database user
     * @return The MySql Connection
     * @throws SQLException
     */
    public static Connection getMySqlConnection(String hostname, String database, String username, String password) throws SQLException {
        String connectionURL = "jdbc:mysql://" + hostname + ":" + port + "/" + database 
                + "?autoReconnect=true&useSSL=false";
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        Connection connection = DriverManager.getConnection(connectionURL, username, password);
        return connection;
        
    }
}
