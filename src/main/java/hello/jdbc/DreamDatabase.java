package hello.jdbc;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import hello.beans.Entry;
import hello.beans.User;
import hello.misc.Time;

/**
 * This class takes care of all of the MySql database operations for the webapp 
 * 
 * @author lcalderas16
 */

public class DreamDatabase {

    public static void main(String[] args) throws SQLException {
        //        User leo = new User("Leopoldo", "Calderas", "pistachios7@gmail.com", "leo", "waffles");
        //        addUser(leo);
        //        createUserTable(leo);
        //        System.out.println(userExists(leo));
        //        System.out.println(Time.getJournalDate());
        //        System.out.println(dailyEntryExists("leo_waffles"));
    }

    /**
     * Checks to see if a user exists in the database
     * @param user The user to verify the existence of in the database
     * @return True if the user is in the database, False otherwise
     * @throws SQLException
     */
    public static boolean userExists(User user) throws SQLException {
        Connection connection = MySqlConnection.getMySqlConnection();

        String sql = "SELECT * FROM login WHERE userName= \"" + user.getUserName() + 
                "\" and breakfast= \"" + user.getBreakfast() + "\";";

        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(sql);

        return rs.next();
    }

    /**
     * Adds a new user to the database.
     * @param user The user to add to the database
     * @return True if the user was successfully added to the database, false otherwise
     * @throws SQLException
     */
    public static boolean addUser(User user) throws SQLException {
        Connection connection = MySqlConnection.getMySqlConnection();

        if (!userExists(user)) {
            String LOGIN_TABLE = "login";
            String sql = "INSERT INTO " + LOGIN_TABLE + " VALUES (\"" + user.getFirstName() + "\", \"" + user.getLastName() + "\", \""
                    + user.getEmail() + "\", \"" + user.getUserName() + "\", \"" + user.getBreakfast() + "\");";
            Statement statement = connection.createStatement();
            int rs = statement.executeUpdate(sql);
            System.out.println("now for the table");
            boolean tableCreated = createUserTable(user);
            System.out.println("Table Created: " + tableCreated);
            if (rs == 1 && tableCreated) {
                return true;
            }
        }
        return false;
    }

    /**
     * Creates a dream entry table for a new user in the database. The user must already exist in the database or it won't create the table.
     * @param user The new user
     * @return True if the table was successfully created, false otherwise.
     * @throws SQLException
     */
    public static boolean createUserTable(User user) throws SQLException {
        if (userExists(user)){
            String tableName = user.toString() ;
            Connection connection = MySqlConnection.getMySqlConnection();

            String sql = "CREATE TABLE " + tableName + 
                    " (date VARCHAR(255) NOT NULL, feeling VARCHAR(255) NOT NULL, didDream VARCHAR(3) NOT NULL, entry TEXT NOT NULL);";
            Statement statement = connection.createStatement();
            int updated = statement.executeUpdate(sql);
            if (updated == 0) {
                return true;
            }
        }
        return false;

    }

    /**
     * Add a dream entry for the user into the database for the current day
     * @param userID The ID of the user
     * @param date The date of the current day
     * @param feeling The response to the question asking how the user is feeling
     * @param didDream The response to the question asking the user if he or she dreamed last night
     * @param entry The response to the question asking the user about the contents of his or her dream
     * @return True if the entry was successfully added to the database, false otherwise
     * @throws SQLException
     */
    public static boolean addEntry(String userID, String date, String feeling, String didDream, String entry) throws SQLException {
        Connection connection = MySqlConnection.getMySqlConnection();

        String sql = "INSERT INTO " + userID + " VALUES (\"" +  date + "\", \"" + feeling + "\", \"" + didDream + "\", \"" + entry + "\");";
        Statement statement = connection.createStatement();
        int rs = statement.executeUpdate(sql);

        if (rs == 1) {
            return true;
        }
        else {
            return false;
        }
    }

    /**
     * Check if a user has already submitted a dream entry for the current day
     * @param userID The ID of the user
     * @return True if the user has already submitted a dream entry for the current day, false otherwise
     * @throws SQLException
     */
    public static boolean dailyEntryExists(String userID) throws SQLException {
        Connection connection = MySqlConnection.getMySqlConnection();

        String sql = "Select * from " + userID + " where date= \"" + Time.getJournalDate() + "\";";
        Statement statement = connection.createStatement();
        ResultSet rs = statement.executeQuery(sql);

        while (rs.next()) {
            return true;
        }
        return false;
    }

    /**
     * Get the current day's entry for a user
     * @param userID The user ID of the user
     * @return The current day's entry for the user
     * @throws SQLException
     */
    public static Entry getTodaysEntry(String userID) throws SQLException {
        if (dailyEntryExists(userID)) {
            Connection connection = MySqlConnection.getMySqlConnection();

            String sql = "Select feeling, didDream, entry from " + userID + " where date= \"" + Time.getJournalDate() + "\";";
            Statement statement = connection.createStatement();
            ResultSet rs = statement.executeQuery(sql);

            while (rs.next()) {
                String feeling = rs.getString(1);
                String didDream = rs.getString(2);
                String entry = rs.getString(3);
                Entry todaysEntry = new Entry(feeling, didDream, entry);
                return todaysEntry;
            }
        }
        // Return empty entry if none found.
        return new Entry("", "", "");
    }
    
    
    public static boolean removeTodaysEntry(String userID) throws SQLException { 
        Connection connection = MySqlConnection.getMySqlConnection();
        
        String sql = "delete from " + userID + " where date= \"" + Time.getJournalDate() + "\";";
        Statement statement = connection.createStatement();
        int rs = statement.executeUpdate(sql);
        if (rs == 1) {
            return true;
        }
        return false;
    }
    
    public static boolean replaceTodaysEntry(String userID, Entry newEntry) throws SQLException {
        removeTodaysEntry(userID);
        boolean added = addEntry(userID, Time.getJournalDate(), newEntry.getFeeling(),
                newEntry.getDidDream(), newEntry.getEntry());
        return added;
    }
}

