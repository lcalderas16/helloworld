package hello.misc;

import java.util.Enumeration;

import javax.servlet.http.*;
import hello.beans.*;

/**
 * This class handles all of the logging in and cookie operations that must be accounted for.
 * @author lcalderas16
 *
 */

public class Util {

    /**
     * Stores the cookie corresponding to the currently logged in User
     * @param response The current response
     * @param user The user that is logged in or logging in
     */
    public static void storeUserCookie(HttpServletResponse response, User user) {
        Cookie userCookie = new Cookie("user", user.toString());
        response.addCookie(userCookie);
    }

    /**
     * Deletes the current cookie associated with the currently logged in user
     * @param response The current response
     */
    public static void deleteUserCookie(HttpServletResponse response) {
        Cookie userCookie = new Cookie("user", "");
        userCookie.setMaxAge(0);
        response.addCookie(userCookie);
    }

    /**
     * Gets the username of the user represented by the current session's logged-in-user cookie
     * @param request The current request
     * @return The username of the logged in user, or an empty string if no user is logged in.
     */
    public static String getUserCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies.length > 0) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("userName")) {
                    return cookie.getValue();
                }
            }
        }
        return "";
    }

    /**
     * Sets the currently logged in user for the current session
     * @param session The current session
     * @param user The currently logged in user
     */
    public static void setLoggedInUser(HttpSession session, User user) {
        session.setAttribute("userLoggedIn", user);
    }

    /**
     * Gets the user ID of the currently logged in user
     * @param session The current session
     * @return The user ID of the currently logged in user
     */
    public static String getLoggedInUser(HttpSession session) {
        Enumeration<String> attributeNames = session.getAttributeNames();
        while (attributeNames.hasMoreElements()) {
            if (attributeNames.nextElement().equals("userLoggedIn")) {
                User loggedInUser = (User) session.getAttribute("userLoggedIn");
                return loggedInUser.toString();
            }
        }
        return "";
    }
}
