package hello.misc;

import java.time.LocalDateTime;
import java.time.format.TextStyle;
import java.util.Locale;

/**
 * This class simply takes care of getting the date for the journal
 * 
 * @author lcalderas16
 *
 */
public class Time {
    
    /**
     * Get the date of the previous day, serving as the date of the night before the current day
     * @return the date of the previous night as a String in the format "Day of the Week, M D, Y"
     */
    public static String getJournalDate() {
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime lastNight = now.minusDays(1);
        String dayOfWeek = lastNight.getDayOfWeek().getDisplayName(TextStyle.FULL, Locale.US);
        String month = lastNight.getMonth().getDisplayName(TextStyle.FULL, Locale.US);
        int dayOfMonth = lastNight.getDayOfMonth();
        int year = lastNight.getYear();
       
        String nowString = "Night of " + dayOfWeek + ", " + month + " " + dayOfMonth + ", " + year;
        return nowString;
    }
    
}
