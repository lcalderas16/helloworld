package hello.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hello.misc.Time;
import hello.misc.Util;
import hello.jdbc.*;

/**
 * Servlet implementation class Entry
 */
@WebServlet("/EntryServlet")
public class EntryServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public EntryServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

        String userID = Util.getLoggedInUser(session);
        String feeling = request.getParameter("feeling");
        String didDream = request.getParameter("didDream");
        String entry = request.getParameter("entry");
        String date = Time.getJournalDate();



        try {
            DreamDatabase.addEntry(userID, date, feeling, didDream, entry);
        } catch (SQLException e) {
            response.sendRedirect("/first/error-page");
            e.printStackTrace();
        }


        if (didDream.equals("yes")) {
            response.sendRedirect("results.jsp");
        } else {
            response.sendRedirect("n-results.jsp");
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
