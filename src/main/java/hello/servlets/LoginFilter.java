package hello.servlets;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hello.beans.User;
import hello.misc.Util;

/**
 * Servlet implementation class LoginFilter
 */
@WebFilter(urlPatterns={"/*"})
public class LoginFilter implements Filter {
    /**
     * @see HttpServlet#HttpServlet()
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        System.out.println("LogIn Filter Initialized!");
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        HttpSession session = req.getSession();
        
        // Check if there's a cookie
        String userCookieValue = Util.getUserCookie(req);
        // There's a cookie!
        if (!(userCookieValue.equals(""))) {
            // Not logged in
            String[] userInfo = userCookieValue.split("_");
            String userName = userInfo[0];
            String userBreakfast = userInfo[1];
            User user = new User(userName, userBreakfast);
            Util.storeUserCookie(resp, user);
            Util.setLoggedInUser(session, user);
        }
        // No cookie...
        else {
            String uri = req.getRequestURI();
            System.out.println(uri);
            System.out.println("Session?: " + Util.getLoggedInUser(session));
            System.out.println("No user in session: " + Util.getLoggedInUser(session).equals(""));
            System.out.println("At a logged out unpermitted page: " + 
                    !(uri.endsWith("login") || uri.endsWith("new-user") ||
                      uri.endsWith("LoginServlet") || uri.endsWith("first/") || 
                      uri.endsWith("new-user-servlet") || uri.endsWith("error-page")));
            System.out.println("Redirect to login: " + (Util.getLoggedInUser(session).equals("") && 
                    !(uri.endsWith("login") || uri.endsWith("new-user") || 
                      uri.endsWith("LoginServlet") || uri.endsWith("first/") || 
                      uri.endsWith("new-user-servlet") || uri.endsWith("error-page"))));
            
            // No user logged in and not at a permitted login/new-user page --> redirect
            if (Util.getLoggedInUser(session).equals("") && 
                    !(uri.endsWith("login") || uri.endsWith("new-user") || 
                      uri.endsWith("LoginServlet") || uri.endsWith("first/") ||
                      uri.endsWith("new-user-servlet") || uri.endsWith("error-page"))) {
                System.out.println("WTF URI IS: " + uri);
                resp.sendRedirect("/first/login");
            }
        }
        chain.doFilter(req, resp);
    }

    @Override
    public void destroy() {
        System.out.println("LogIn Filter Destroyed!");
    }

}
