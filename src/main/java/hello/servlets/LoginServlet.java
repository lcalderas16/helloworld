package hello.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hello.beans.User;
import hello.jdbc.DreamDatabase;
import hello.misc.Util;

/**
 * Servlet implementation class LoginServlet
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String breakfast = request.getParameter("password");
        User user = new User(username, breakfast);
        boolean userExists = false;
        HttpSession session = request.getSession();
        // if user exists, create cookie, set login attributes
        try {
            userExists = DreamDatabase.userExists(user);
        } catch (SQLException e) {
            response.sendRedirect("/first/error-page");
            e.printStackTrace();
        }
        if (userExists) {
            session.setAttribute("loginSuccess", true);
            Util.setLoggedInUser(session, user);
            // Create cookie for user
            Util.storeUserCookie(response, user);
            response.sendRedirect("/first/entry");
        } else {
            session.setAttribute("loginSuccess", false);
            response.sendRedirect("/first/login");
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
