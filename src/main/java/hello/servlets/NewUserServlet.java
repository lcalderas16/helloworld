package hello.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hello.beans.User;
import hello.jdbc.DreamDatabase;
import hello.misc.Util;

/**
 * Servlet implementation class NewUserServlet
 */
@WebServlet("/new-user-servlet")
public class NewUserServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public NewUserServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String email = request.getParameter("email");
        String userName = request.getParameter("userName");
        String breakfast = request.getParameter("breakfast");

        User newUser = new User(firstName, lastName, email, userName, breakfast);
        boolean userCreated = false;
        
        try {
            //TODO concurrency and shit
            userCreated = DreamDatabase.addUser(newUser);
        } catch (SQLException e) {
            response.sendRedirect("/first/error-page");
        }
        if (userCreated){
            HttpSession session = request.getSession();
            // Set session, cookie, user info for auto login
            session.setAttribute("loginSuccess", true);
            Util.setLoggedInUser(session, newUser);
            // Create cookie for user
            Util.storeUserCookie(response, newUser);
            System.out.println("SEE THIS IF IT'S HEADED TO ENTRY PAGE");
            response.sendRedirect("/first/entry");
        } else {
            response.sendRedirect("/first/login");
        }


    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
