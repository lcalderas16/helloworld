package hello.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hello.beans.Entry;
import hello.jdbc.DreamDatabase;
import hello.misc.Util;

/**
 * Servlet implementation class EntryExistsServlet
 */
@WebServlet("/EntryExistsServlet")
public class EntryExistsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public EntryExistsServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    String answer = request.getParameter("answer");
	    HttpSession session = request.getSession();
	    String userID = Util.getLoggedInUser(session);
	    Entry todaysEntry;
        try {
            todaysEntry = DreamDatabase.getTodaysEntry(userID);
            request.getSession().setAttribute("userID", userID);
            request.getSession().setAttribute("todaysEntry", todaysEntry);
        } catch (SQLException e) {
            response.sendRedirect("/first/error-page");
            e.printStackTrace();
        }
	    
	    if (answer.equals("yes")) {
	        response.sendRedirect("redo-entry");
	    }
	    else {
	        response.sendRedirect("login");
	    }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
