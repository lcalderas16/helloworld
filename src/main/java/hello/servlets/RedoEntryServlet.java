package hello.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hello.misc.Util;
import hello.beans.Entry;
import hello.jdbc.*;

/**
 * Servlet implementation class Entry
 */
@WebServlet("/RedoEntryServlet")
public class RedoEntryServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public RedoEntryServlet() {
        super();
    }

    /**
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        
       

        String userID = Util.getLoggedInUser(session);
        String feeling = request.getParameter("feeling");
        String didDream = request.getParameter("didDream");
        String entry = request.getParameter("entry");
        Entry newEntry = new Entry(feeling, didDream, entry);
        try {
            DreamDatabase.replaceTodaysEntry(userID, newEntry);
        } catch (SQLException e1) {
            response.sendRedirect("/first/error-page");
            e1.printStackTrace();
        }

        if (didDream.equals("yes")) {
            response.sendRedirect("results.jsp");
        } else {
            response.sendRedirect("n-results.jsp");
        }
    }

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

}
