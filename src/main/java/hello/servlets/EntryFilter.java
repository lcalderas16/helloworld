package hello.servlets;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import hello.jdbc.DreamDatabase;
import hello.misc.Util;

/**
 * Servlet Filter implementation class EntryFilter
 */
@WebFilter(urlPatterns={"/entry"})
public class EntryFilter implements Filter {

    /**
     * Default constructor. 
     */
    public EntryFilter() {
    }

    /**
     * @see Filter#init(FilterConfig)
     */
    public void init(FilterConfig fConfig) throws ServletException {
    }


    /**
     * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
     */
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        
        HttpSession session = req.getSession();
        String userID = Util.getLoggedInUser(session);
        
        try {
            if (DreamDatabase.dailyEntryExists(userID)) {
                resp.sendRedirect("entry-exists.jsp");
            }
        } catch (SQLException e) {
            resp.sendRedirect("/first/error-page");
            e.printStackTrace();
        }
        chain.doFilter(request, response);
    }

    /**
     * @see Filter#destroy()
     */
    public void destroy() {
    }

}
