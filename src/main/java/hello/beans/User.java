package hello.beans;
/**
 * 
 * @author lcalderas16
 *
 * A User of the Dream Journal
 */
public class User {
    
    String firstName;
    String lastName;
    String email;
    String userName;
    String breakfast;
    
    public User(String firstName, String lastName, String email, String userName, String breakfast) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.userName = userName;
        this.breakfast = breakfast;
        this.email = email;
    }
    
    public User(String userName, String breakfast) {
        this.userName = userName;
        this.breakfast = breakfast;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getBreakfast() {
        return breakfast;
    }

    public void setBreakfast(String breakfast) {
        this.breakfast = breakfast;
    }
    
    public String toString() {
        return this.getUserName() + "_" + this.getBreakfast();
    }

    
}