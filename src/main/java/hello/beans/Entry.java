package hello.beans;

public class Entry {
    String feeling;
    String didDream;
    String entry;
    
    public Entry(String feeling, String didDream, String entry) {
        this.feeling = feeling;
        this.didDream = didDream;
        this.entry = entry;
    }

    public String getFeeling() {
        return feeling;
    }

    public void setFeeling(String feeling) {
        this.feeling = feeling;
    }

    public String getDidDream() {
        return didDream;
    }

    public void setDidDream(String didDream) {
        this.didDream = didDream;
    }

    public String getEntry() {
        return entry;
    }

    public void setEntry(String entry) {
        this.entry = entry;
    }
    
    
}
